module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
          {
              expand: true,
              cwd: 'sources/views/',
              src: '**',
              dest: 'www/'
          },

					{
						expand: true,
						cwd: 'sources/fonts',
						src: '**',
						dest: 'www/fonts'
					},
					{
						expand: true,
						cwd: 'sources/images',
						src: '**',
						dest: 'www/images'
					},
					{
						expand: true,
						cwd: 'sources/scripts',
						src: '**',
						dest: 'www/scripts'
					},
					{
						expand: true,
						cwd: 'sources/styles',
						src: '**/*.css',
						dest: 'www/styles'
					},
					{
						expand: true,
						cwd: 'sources/styles/images',
						src: '**',
						dest: 'www/styles/images'
					},
					{
						expand: true,
						cwd: 'node_modules/jquery/dist',
						src: 'jquery.min.js',
						dest: 'www/scripts/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/owl.carousel/dist',
						src: 'owl.carousel.min.js',
						dest: 'www/scripts/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/owl.carousel/dist/assets',
						src: 'owl.carousel.min.css',
						dest: 'www/styles/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/font-awesome/fonts',
						src: '**',
						dest: 'www/fonts'
					},
					{
						expand: true,
						cwd: 'node_modules/font-awesome/css',
						src: 'font-awesome.min.css',
						dest: 'www/styles'
					},
					{
						expand: true,
						cwd: 'node_modules/bootstrap/dist/js',
						src: 'bootstrap.bundle.min.js',
						dest: 'www/scripts/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/waypoints/lib',
						src: 'jquery.waypoint.min.js',
						dest: 'www/scripts/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/aos/dist',
						src: 'aos.js',
						dest: 'www/scripts/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/bootstrap/dist/css',
						src: 'bootstrap.min.css',
						dest: 'www/styles/plugins'
					},
					{
						expand: true,
						cwd: 'node_modules/aos/dist',
						src: 'aos.css',
						dest: 'www/styles/plugins'
					}

				]
			}
		},
		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					sourceMap: true
				},
				files: [{
					expand: true,
					cwd: 'sources/styles/',
					src: ['**/*.sass'],
					dest: 'www/styles/',
					ext: '.css',
					extDot: 'last'
				}]
			}
		},
		watch: {
			css: {
				files: 'sources/styles/**/*.css',
				tasks: ['copy']
			},
			fonts: {
				files: 'sources/fonts/**/*',
				tasks: ['copy']
			},
			files: {
				files: 'sources/files/**/*',
				tasks: ['copy']
			},
			images: {
				files: 'sources/images/**/*',
				tasks: ['copy']
			},
			sass: {
				files: 'sources/styles/**/*.sass',
				tasks: ['sass']
			},
			scripts: {
				files: 'sources/scripts/**/*.js',
				tasks: ['copy']
			},
			views: {
				files: 'sources/views/*.html',
				tasks: ['copy']
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-pug');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');

	grunt.registerTask('default', ['watch']);
	grunt.registerTask('build', ['copy','sass']);
};
