$( document ).ready(function() {

			$('.btn-outline-success').on('click', function(e){
					e.preventDefault();
					$(".form-control").toggleClass('search');
				});


		 $('.card-header').on('click', function(){
			 if ($(this).hasClass('opened')) {
				 $(this).removeClass('opened');
			 } else {
			$(".card-header").removeClass('opened');
		  $(this).addClass('opened');
			}
		});


		$('#owl-carousel').owlCarousel({
		loop: true,
		dots: true,
    autoplay: true,
    items: 1,
    nav: true,
    autoplayTimeout: 5000,
		autoplaySpeed: 1000,
    autoplayHoverPause: true
	});

	$('#small-carousel').owlCarousel({
	loop: true,
	margin: 3,
	dots: true,
	autoplay: true,
	items: 3,
	nav: true,
	autoplayTimeout: 5000,
	autoplaySpeed: 1000,
	autoplayHoverPause: true,
	responsive: {
		767: {
			items: 3
		},
		991: {
			items: 1
		},
		1200: {
			items: 3
		}
	}
	});

	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		    if (scroll < 300)
		     {
		        $('#scrollUp').hide();
		     }
		    else
		     {
		      $('#scrollUp').show();
	     }
		 	});

			AOS.init();

			var waypoint = new Waypoint({
				element: document.getElementById('run'),
				handler: function() {
				 $('.stat:first-child').addClass('run');
				 $('.stat:nth-child(2)').addClass('run2');
				 $('.stat:nth-child(3)').addClass('run3');
				 $('.stat:nth-child(4)').addClass('run4');
				}
			});


});
